import os

#Menu
def menu(): 
    os.system('cls')
    print('1) Registrar productos.')
    print('2) Mostrar listado productos.')
    print('3) Mostrar productos con stock dentro de un intervalo[desde-hasta].')
    print('4) Sumar X al stock de productos con stock menor a Y.')
    print('5) Eliminar productos con stock igual a 0.')
    print('6) Salir.')
    opc =  int(input('Ingrese una opcion: '))
    while (not (opc>=1) and (opc<=6)):
        opc =  int(input('Ingrese una opcion: '))
    return  opc

#1) Registrar productos a partir de codigo, descripcion, precio y stock.
def cargarProductos():
    print('Carga de lista de productos.')
    cont = 's'
    while (cont == 's'):
        codigo = int(input('Ingrese codigo de producto: '))
        if codigo not in productos:
            descripcion = input('Descripcion: ')
            precio = leerPrecio()
            stock = leerStock()
            productos[codigo] = [descripcion, precio, stock]
            print('Producto agregado!')
        else:
            print('Producto existente.') 
        cont = input('Desea continuar ingresando? s/n: ')
    return productos

#Validar precio positivo
def leerPrecio():
    x = float(input('Precio: '))
    while (x<0):
        print('El precio tiene que ser mayor que 0!')
        x= float(input('Precio: '))
    return x

#Validar stock mayor o igual a 0
def leerStock():
    x = int(input('Cantidad: '))
    while (x<0):
        print('No puede haber stock negativo!')
        x = int(input('Cantidad: '))
    return x

#2) Mostrar listado de productos
def mostrarProductos(productos):
    for clave, valor in productos.items():
        print(clave, valor)

def encabezado():
    print('  |Descripcion|Precio|Stock|')

#3) Mostrar productos con stock dentro de un intervalo ingresado por teclado.
def DesdeHasta(productos):
    desde = int(input('Stock desde: '))
    hasta = int(input('Stock hasta: '))
    for codigo, datos in productos.items():
        if (datos[2] >= desde and datos[2] <= hasta):
            print(codigo, datos)

#4)Aumentar X en stock menores a Y.
def sumarX(productos):
    x = int(input('Valor X para sumar: '))
    y = int(input('Stock menor a: '))
    for clave, valor in productos.items():
        if (valor[2]<y):
            stock = valor[2] + x
            productos[clave][2] = stock

#5) Eliminar productos con stock igual a 0.
def eliminar0(productos):
    productosAux = productos.copy()
    for clave, valor in productosAux.items():
        if (valor[2]==0):
            productos.pop(clave)
            
#PRINCIPAL
productos = {}
opc = 0
ingreso = False  #Verifica la carga de productos.
while (opc!=6):
    opc = menu()
    if (opc==1):
        productos = cargarProductos()
        ingreso = True
    elif (opc==2):
        if (ingreso):
            encabezado() 
            mostrarProductos(productos)
        else:
            print('NINGUN PRODUCTO HA SIDO CARGADO AÚN.')
    elif (opc==3): 
        if (ingreso):
            DesdeHasta(productos)
        else:
            print('NINGUN PRODUCTO HA SIDO CARGADO AÚN.')
    elif (opc==4):
        if (ingreso):
            sumarX(productos)
        else:
            print('NINGUN PRODUCTO HA SIDO CARGADO AÚN.')
    elif (opc==5):
        if (ingreso):
            eliminar0(productos)
        else:
            print('NINGUN PRODUCTO HA SIDO CARGADO AÚN.')
    elif (opc==6):
        print('Saliendo...')
    input('Enter para continuar')
